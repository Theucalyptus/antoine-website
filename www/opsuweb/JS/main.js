//Données gestion statut jeux
var paused = false;
var playing = false;
var started = false;
var allow_skip = false;

//Données pour la sélection des beatmaps
var beatmap = null;
var selectedIndex = 4;
var beatmapList = [[3, 'map'], [2, 'map'], [1, 'map'], [4, 'map'], [5, 'map'], [6, 'test']];
var beatmap_loaded = false;

//Données gestion IO 
var touche1 = false;
var touche2 = false;

//Données audio/images à précharger
var audio; //sons
var hitSounds = []
var hitSoundsCounter = 0;
var sonBreak;
var pauseRetryClick;
var pauseBackClick;
var pauseContinue;
var startPlay;
var sonSkip;

var logo_img;
var pause_back;
var pause_continue;
var pause_retry;

var imageCercleBase; //images in-game
var imageCercleOverlay;
var imageCercleApproche;
var imagesGrades;
var imagesHitResult;
var imageSkip;

//Données pour le timing
var lastUpdateTime = 0;
var tempsEnJeu = 0;


//Données pour le suivi durant une partie
var indexPremierCercleAffiché = 0;
var indexCercleAUpdate = 0;
var objetsAafficher;
var objetAupdate;

//Données pour le placement des points à l'écran
var xOffset;
var yOffset;
var xMultiplier;
var yMultiplier;
var baseSize;

//Données pour le score, les combo et la vieux
var vie;
var score;

//Autres
var couleursCombo = []; //liste de p5.Color pour colorier les cerlces
var particules = []; //liste de particule pour afficher les hitResult
var sliderColor;

//FONCTIONS DE CHARGEMENT ET DÉMARRAGE DU JEU
// préchargement des images et sons indépendant à la beatmap
function preload() {	
	
	soundFormats('mp3', 'ogg');
	//Musique du menu
	audio = new Audio("Data/menu-musique.mp3");
	audio.loop = true;

	for (let x=0;x<10;x++) {
		sfx = loadSound('Data/normal-hitnormal.ogg')
		hitSounds.push(sfx);
	}		
	sonBreak = loadSound('Data/combobreak.ogg');
	pauseBackClick = loadSound('Data/pause-back-click.ogg');
	pauseRetryClick = loadSound('Data/pause-retry-click.ogg');
	pauseContinueClick = loadSound('Data/pause-continue-click.ogg');
	startPlay = loadSound('Data/menu-play-click.ogg');
	sonSkip = startPlay;

	//Images du menu
	logo_img = loadImage('Data/logo.png');

	//Image bouton menu pause
	pause_back = loadImage('Data/pause-back.png');
	pause_retry = loadImage('Data/pause-retry.png');
	pause_continue = loadImage('Data/pause-continue.png');


	//Image pour le jeu 
	imageCercleApproche = loadImage('Data/approachcircle.png');
	imageCercleOverlay = loadImage('Data/hitcircleoverlay.png');
	imageCercle = loadImage('Data/hitcircle.png');
	imagesGrades = [loadImage('Data/ranking-d-small.png'), loadImage('Data/ranking-c-small.png'), loadImage('Data/ranking-b-small.png'), loadImage('Data/ranking-a-small.png'), loadImage('Data/ranking-s-small.png'), loadImage('Data/ranking-sh-small.png'), loadImage('Data/ranking-x-small.png'), loadImage('Data/ranking-xh-small.png')];
	imagesHitResult = [loadImage('Data/hit300.png'), loadImage('Data/hit100.png'), loadImage('Data/hit50.png'), loadImage('Data/hit0.png')]
	imageSliderFollowCircle = loadImage('Data/sliderfollowcircle.png');
	imageSliderScorePoint = loadImage('Data/sliderscorepoint.png');
	imageSkip = loadImage("Data/play-skip.png")


	sliderColor = color(120,120,120, 125);
}
//focntion qui créer le canvas P5 et creer les boutons 
function setup() {
	
	noLoop();
	document.getElementById("alert").innerText = "";

	let mycanvas = createCanvas(windowWidth, windowHeight);
	mycanvas.parent("context");

	bouton_jouer = createButton("Jouer");
	bouton_jouer.id('bouton-jouer');
	bouton_jouer.mousePressed(start);

	document.getElementById("start_text").innerText = " !! ATTENTION !! Ce projet est encore lourdement en développement et est par conséquent assez brut de décoffrage. Toutes les fonctionnalitées n'ont pas encore été implémentées et celles existantes n'ont que peu été testées. Toute les beatmaps ne sont donc pas encore fonctionnelles. Merci de votre compréhension. \n \n Recommandations d'utilisation : \n - Un navigateur basé sur Chromium est fortement recommandé pour des raisons de performances. \n - Une prise en charge d'accélération matierielle est nécessaire pour obtenir des performances convenables. \n - Le port d'un casque ainsi que l'utilsiation d'une souris (et non un trackpad) sont fortement recommandés. \n \n Mode d'emploi : \n - Touches : V pour choisir la Beatmap dans le menu, 'Entrée' pour jouer la Beatmap actuellement sélectionnée (elles sont rangées de la plus simple à la plus dure),  P pour mettre en pause et **W,X pour toucher les notes** en cours de jeux \n \n Explications : \n Osu! est un jeu de rythme musical. Des cercles ainsi que des cercles d'approche concentriques apparaissent en rythme avec la musique. L'objectif est d'**appuyer** sur les cercles au moment au le cerlce d'approche fusione avec ce dernier, et ce en visant le cercle avec le curseur. \n La précision du timing est prise en compte et impact le score. Le nombre de cercle obtenu d'affiler augment le combo qui agit comme un multiplicateur de score. \n Le grade obtenu (SS,S,A,B...) dépend de la précision, à l'exception de SS qui s'obitent en faisant le score parfait (100% de précision) et S en ne manquant aucune note. \n Les sliders sont un type particulier de cercle : il faut maintenir la touche du clavier enfoncée et suivre le chemin sans sortir du cercle de suivi. Certain on un point au bout: cela signfie qu'il faut faire un aller-retour et non un aller-simple. \n Vous disposez d'une barre de vie : si cette dernière tombre à 0, vous avez perdu. Si vous survivez jusqu'à la fin, bravo : vous avez clear cette map. \n \n Objectif: \n L'objectif de ce projet est la création d'un client osu! sous le format d'un site web capable de jouer n'importe quelle beatmap en répliquant à l'exacte le comportement du jeu officiel. Il se base pour cela du code du Opsu, un client open-source codée en Java. \n Il manque encore pour avoir un gameplay équivalent : les spinners, les slider beziers, la prise en charge des pauses et les hitsounds custom et non-custom. \n \n Crédits: Opsu team, Antoine L.,  développé avec beaucoup d'amour. <3 \n Contact: u/Theucalyptus";



}
//lorsque l'utilisateur clique sur le bouton jouer
function start() {

	started = true;
	bouton_jouer.hide();
	elems = document.getElementsByClassName("ui");
	for (let i = 0; i < elems.length; i++) {
		elems[i].classList.add('started');
	}
	fullscreen(true);
	document.getElementById('start_text').innerText = "";
	document.getElementById("context").classList.add('started');
	document.getElementById('context').style.backgroundImage = "url('Data/menu-background.jpg')";

	score = new Score();
	vie = new Vie();

	audio.play();
	loop();

}



// FONCTIONS DE GESTION DU CLAVIER ET DE LA SOURIS

function keyPressed() {
	if (keyCode === 80 && playing == true) { // si p est pressé = mise en pause/relance du jeu
		paused = true
		audio.pause();
	}
	else if (keyCode === 87) { //si la touche 1 'w' est pressée
		touche1 = true;
	}
	else if (keyCode === 88) { //si la touche 2 'x' est pressée
		touche2 = true;
	}
	else if (keyCode === 13) { //si entré est pressée
		if(playing == false) {
			startSelectedBeatmap();
		}
	}
	else if (keyCode == 86) {

		selectedIndex++;
		if (selectedIndex == beatmapList.length) {
			selectedIndex = 0;
		}
		loadSelectedBeatmap();
	}
	
	else if(keyCode == 32  && allow_skip == true && beatmap.getObjet(0).time -tempsEnJeu > 2500 && tempsEnJeu > 0) { //skip en début de map
		tempsEnJeu = beatmap.getObjet(0).time-2000;
		lastUpdateTime = d.getTime();
		audio.currentTime = tempsEnJeu/1000;
		allow_skip = false;
		sonSkip.play();
	}

	if (playing == true && paused == false) {
		UpdateHitObjects()
	}


}
function keyReleased() {

	if (keyCode === 87) { //si la touche 1 'w' est relachée
		touche1 = false;
	}
	else if (keyCode === 88) { //si la touche 2 'x' est relachée
		touche2 = false;

	}
}
function mousePressed() {

	if (paused == true) {
		if (mouseX >= windowWidth * 0.6 && mouseX <= windowWidth * 0.6 + pause_back.width && mouseY >= windowHeight * 5 / 16 && mouseY <= windowHeight * 5 / 16 + pause_back.height) {
			paused = false;
			playing = false;
			document.getElementById("bas-gauche").innerHTML = "";
			loadSelectedBeatmap();
			pauseBackClick.play();
		}
		else if (mouseX >= windowWidth * 0.6 && mouseX <= windowWidth * 0.6 + pause_retry.width && mouseY >= windowHeight * 8 / 16 && mouseY <= windowHeight * 8 / 16 + pause_retry.height) {
			paused = false;
			startSelectedBeatmap();
			pauseRetryClick.play();
		}
		else if (mouseX >= windowWidth * 0.6 && mouseX <= windowWidth * 0.6 + pause_continue.width && mouseY >= windowHeight * 11 / 16 && mouseY <= windowHeight * 11 / 16 + pause_continue.height) {
			paused = false;
			lastUpdateTime = new Date().getTime();
			audio.play();
			pauseContinueClick.play();
		}
	}

}

// FONCTION D'INTERACTION DU JEU
//charge la beatmap sélectionnée
function loadSelectedBeatmap() {

	beatmap = new BeatMap(beatmapList[selectedIndex][0], beatmapList[selectedIndex][1] + '.osu'); //chargement beatmap
	audio.pause();

	beatmap_loaded = false;



	document.addEventListener("beatmap_loaded", function (event) {
		audio = beatmap.musique
		audio.play();
		document.getElementById('context').style.backgroundImage = "url('" + beatmap.background + "')";

		couleursCombo = beatmap.couleursCombo;

		document.getElementById('haut-droit').innerHTML = beatmap.titre + " - " + beatmap.artiste;
		document.getElementById('haut-droit2').innerHTML = beatmap.version + " [" + beatmap.createur + "]";
		document.getElementById('bas-gauche').innerText = ""

		baseSize = beatmap.diametreCercle * xMultiplier;

		vie.Init(beatmap);

		beatmap_loaded = true;
	});

}
//lance une partie sur la beatmap sélectionnée
function startSelectedBeatmap() {

	startPlay.play()
	
	
	if (beatmap_loaded == true) {
		audio.pause();
		time = new Date().getTime();
		lastUpdateTime = time;

		document.getElementById('context').style.backgroundImage = '';

		indexPremierCercleAffiché = 0;
		indexCercleAUpdate = 0;

		vie.Reset();
		score.Reset()

		particules = [];

		if (beatmap.offsetAudio > 2000) {
			tempsEnJeu = -beatmap.offsetAudio;
		}
		else {
			tempsEnJeu = -2000;
		}

		if(beatmap.getObjet(0).time - tempsEnJeu >= 4000) {
			allow_skip = true;
		}
		else {
			allow_skip = false;
		}

		playing = true;
		audio.currentTime = 0;
	}

}
//en cours de partie: fait les updates nécessaires
function Update() {
	d = new Date();
	
	if (paused == false) {
		tempsEnJeu = tempsEnJeu + (d.getTime() - lastUpdateTime);
		lastUpdateTime = d.getTime();


		if (tempsEnJeu >= beatmap.getObjet(0).time) {
			vie.UpdatePassive(tempsEnJeu);
		}
		
		let lastHitObject = beatmap.getObjet(beatmap.hitObjectsLength-1);
		if (vie.vie <= 0 || tempsEnJeu > lastHitObject.time + lastHitObject.tempsTotalSlider + 2000) { //si mort ou beatmap finie
			playing = false; //retour au menu
			paused = false;
			document.getElementById('bas-gauche').innerHTML = "";
			loadSelectedBeatmap();

		}
		else {
			if (audio.paused == true) {		
				if (tempsEnJeu >= 0) {
					audio.play();
					tempsEnJeu = 0;
				}
			}
			
			//Mise à jour des HitObjects
			indexPremierCercleAffiché = beatmap.Update(tempsEnJeu, indexCercleAUpdate, indexPremierCercleAffiché);
			objetsAafficher = beatmap.getObjets(indexCercleAUpdate, indexPremierCercleAffiché);
			UpdateHitObjects();

			//Mise à jour des particules
			for (let j = 0; j < particules.length; j++) {
				if (tempsEnJeu - particules[j].time >= 500) {
					particules = particules.splice(0, j - 1).concat(particules.splice(j + 1, particules.length - 1));
				}
			}
		}
	}

}


// AUTRES FONCTIONS

//fonction de mise à jour de la fenètre
function windowResized() {
	resizeCanvas(windowWidth, windowHeight);

	let swidth = windowWidth;
	let sheight = windowHeight;

	if (swidth * 3 > sheight * 4) {
		swidth = sheight * 4 / 3;
	} else {
		sheight = swidth * 3 / 4;
	}



	xMultiplier = swidth / 640;
	yMultiplier = sheight / 480;
	xOffset = (windowWidth - 512 * xMultiplier) / 2;
	yOffset = (windowHeight - 384 * yMultiplier) / 2;

}
//fonction vérfiant si le point est validé
function CercleClickTest() {

	inGameMouseX = (mouseX - xOffset) / xMultiplier; //conversion de la position de la souris en position osu pixel
	inGameMouseY = (mouseY - yOffset) / yMultiplier;
	
	let hitResultsOffset = beatmap.hitResultsOffset;
	let objet = beatmap.getObjet(indexCercleAUpdate);
	let distance = Math.sqrt(Math.pow(inGameMouseX - objet.x, 2) + Math.pow(inGameMouseY - objet.y, 2));
	let timeDiff = Math.abs(tempsEnJeu - objet.time);

		if (distance <= beatmap.diametreCercle / 2) {	//si distance <= diameter/2, alors :

			if (touche1 == true) {
				if (timeDiff <= hitResultsOffset[0]) {
					score.Update(0);
					vie.UpdateClick(0);
					playHitSound();
				}
				else if (timeDiff <= hitResultsOffset[1]) {
					score.Update(1);
					vie.UpdateClick(1);
					particules.push(new Particule(1, tempsEnJeu, objet.x, objet.y));
					playHitSound();
				}
				else if (timeDiff <= hitResultsOffset[2]) {
					score.Update(2);
					vie.UpdateClick(2);
					particules.push(new Particule(2, tempsEnJeu, objet.x, objet.y));
					playHitSound();
				}
				else if (timeDiff <= hitResultsOffset[3]) {
					playBreakSound();
					score.Update(3);
					vie.UpdateClick(3);
					particules.push(new Particule(3, tempsEnJeu, objet.x, objet.y));
				}
				touche1 = false;
				indexCercleAUpdate++;
			}
			else if (touche2 == true) {
				if (timeDiff <= hitResultsOffset[0]) {
					score.Update(0);
					vie.UpdateClick(0);
					playHitSound();
				}
				else if (timeDiff <= hitResultsOffset[1]) {
					score.Update(1);
					vie.UpdateClick(1);
					particules.push(new Particule(1, tempsEnJeu, objet.x, objet.y));
					playHitSound();
				}
				else if (timeDiff <= hitResultsOffset[2]) {
					score.Update(2);
					vie.UpdateClick(2);
					particules.push(new Particule(2, tempsEnJeu, objet.x, objet.y));
					playHitSound();
				}
				else if (timeDiff <= hitResultsOffset[3]) {
					playBreakSound();
					score.Update(3);
					vie.UpdateClick(3);
					particules.push(new Particule(3, tempsEnJeu, objet.x, objet.y));
				}
				touche2 = false;
				indexCercleAUpdate++;

			}

		} else if ((tempsEnJeu - objet.time) > hitResultsOffset[2]) {
			playBreakSound();
			score.Update(3);
			vie.UpdateClick(3);
			indexCercleAUpdate++;
			particules.push(new Particule(3, tempsEnJeu, objet.x, objet.y));
		}
}
function SliderClickTest() {
	
	inGameMouseX = (mouseX - xOffset) / xMultiplier; //conversion de la position de la souris en position osu pixel
	inGameMouseY = (mouseY - yOffset) / yMultiplier;
	
	hitResultsOffset = beatmap.hitResultsOffset;
	objet = beatmap.getObjet(indexCercleAUpdate);

	timeDiff = Math.abs(objet.time - tempsEnJeu)
	distance = Math.sqrt(Math.pow(inGameMouseX - objet.x, 2) + Math.pow(inGameMouseY - objet.y, 2));


	// if (objet.repetition % 2 == 0) {
	// 	pointFin = objet.curve.pointAt(0)
	// }
	// else {
	// 	pointFin = objet.curve.pointAt(1.0)
	// }
	
	// distance2 = Math.sqrt(Math.pow(inGameMouseX - pointFin.x, 2) + Math.pow(inGameMouseY - pointFin.y, 2));
    distance2 = 0;

	if (objet.pointInitialClick == false && distance < beatmap.diametreCercle / 2 && timeDiff < hitResultsOffset[3]) { //click initial du slider		
		if (touche1) {
				
				if (timeDiff <= hitResultsOffset[0]) {
					score.Update(0);
					vie.UpdateClick(0);
					playHitSound();
					beatmap.hitObjects[indexCercleAUpdate].setPointInitialClick(true);
				}
				else if (timeDiff <= hitResultsOffset[1]) {
					score.Update(1);
					vie.UpdateClick(1);
					particules.push(new Particule(1, tempsEnJeu, objet.x, objet.y));
					playHitSound();
					beatmap.hitObjects[indexCercleAUpdate].setPointInitialClick(true);

				}
				else if (timeDiff <= hitResultsOffset[2]) {
					score.Update(2);
					vie.UpdateClick(2);
					particules.push(new Particule(2, tempsEnJeu, objet.x, objet.y));
					playHitSound();
					beatmap.hitObjects[indexCercleAUpdate].setPointInitialClick(true);

				}
				else if(timeDiff >= hitResultsOffset[2]) { 
					playBreakSound();
					score.Update(3);
					vie.UpdateClick(3);
					particules.push(new Particule(3, tempsEnJeu, objet.x, objet.y));
				}

			}
			else if(touche2) {
				beatmap.hitObjects[indexCercleAUpdate].setPointInitialClick(true);

				if (timeDiff <= hitResultsOffset[0]) {
					score.Update(0);
					vie.UpdateClick(0);
					playHitSound();
					beatmap.hitObjects[indexCercleAUpdate].setPointInitialClick(true);

				}
				else if (timeDiff <= hitResultsOffset[1]) {
					score.Update(1);
					vie.UpdateClick(1);
					particules.push(new Particule(1, tempsEnJeu, objet.x, objet.y));
					playHitSound();
					beatmap.hitObjects[indexCercleAUpdate].setPointInitialClick(true);

				}
				else if (timeDiff <= hitResultsOffset[2]) {
					score.Update(2);
					vie.UpdateClick(2);
					particules.push(new Particule(2, tempsEnJeu, objet.x, objet.y));
					playHitSound();
					beatmap.hitObjects[indexCercleAUpdate].setPointInitialClick(true);
				}
				else if(timeDiff >= hitResultsOffset[2]) { 
					playBreakSound();
					score.Update(3);
					vie.UpdateClick(3);
					particules.push(new Particule(3, tempsEnJeu, objet.x, objet.y));
					indexCercleAUpdate++;
				}
			}
	}
	else if (objet.endTime > tempsEnJeu) { //vérif de la distance entre le curseur et le point en cours
	
	
	}
	else if(objet.endTime < tempsEnJeu) { //fin du slider
		indexCercleAUpdate++;	
		if ( (touche1 == true || touche2 == true) &&  distance2 < beatmap.diametreCercle / 2) {
			vie.UpdateClick(0);
			score.Update(0);
			playHitSound();
		}
		else {
			vie.UpdateClick(3);
			score.Update(3);
			playBreakSound();
		}
		touche1 = false;
		touche2 = false;
	}
	    //ajouter support pour les sliders allées-retour;



	
}

function UpdateHitObjects() {
		
	if (indexCercleAUpdate == beatmap.hitObjects.length) {
		indexCercleAUpdate = beatmap.hitObjects.length -1;
	}

	objet = beatmap.getObjet(indexCercleAUpdate);
	hitResultsOffset = beatmap.hitResultsOffset;


		typeObjet = objet.type;
		switch(typeObjet) {
			case 1:
				CercleClickTest();
				break;
			case 2:
				SliderClickTest();
				break;
			case 8:
				indexCercleAUpdate++;
			default:
				break;
		}
	
}
//fonction jouant le son de hit		    	    	
function playHitSound() {
	
	hitSounds[hitSoundsCounter].play();
	hitSoundsCounter++;
	if(hitSoundsCounter >= 10) {
		hitSoundsCounter = 0;
	}
}
//fonction jouant le son de fin de combo
function playBreakSound() {
	if (score.combo >= 20) {
		sonBreak.play();
	}

}


