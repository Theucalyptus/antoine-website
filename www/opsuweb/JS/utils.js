// Fichier contenant des fonctions utiliser à de nombreux endroits divers

function mapDifficultyRange(difficulty, min, mid, max) {
    if (difficulty > 5.0) {
        return mid + (max - mid) * (difficulty - 5.0) / 5.0;
    }
    else if (difficulty < 5.0) {
        return mid - (mid - min) * (5.0 - difficulty) / 5.0;
    }
    else {
        return mid;
    }




}
function scaleX(coordX) {
    return coordX * xMultiplier + xOffset;
}
function scaleY(coordY) {
    return coordY * yMultiplier + yOffset;
}

function VecLerp(pointA, pointB, t) {
    return new Vec2f(lerp(pointA.x, pointB.x, t), lerp(pointA.y, pointB.y, t))
}

class Vec2f { //équivalent allégé de la classe Vec2f de Opsu!
    constructor(x,y) {
        this.X = x;
        this.Y = y;
    }

    get x() {
        return this.X;
    }
    get y() {
        return this.Y;
    }
    get longueur() {
        return Math.hypot(this.X, this.Y);
    }
    
    copy() {
        return  new Vec2f(this.X, this.Y);
    }
    
    add(vecteur) {
        this.X += vecteur.x;
        this.Y += vecteur.y;

        return this;
    }

    sub(vecteur) {
        this.X -= vecteur.x;
        this.Y -= vecteur.y;

        return this;
    }

    equals(vecteur) {
        return (this.X == vecteur.x && this.Y == vecteur.y);
    }
    scale(coef) {
        this.X *= coef;
        this.Y *= coef;

        return this;
    }

    midPoint(vecteur) {
        return new Vec2f( (this.X + vecteur.x) /2 , (this.Y + vecteur.y)/2);
    }

    nor() {
        let nx = -this.Y;
        let ny = this.X;
        this.X = nx;
        this.Y = ny;
        return this;
    }

    setX(value) {
        this.X = value;
    }
    setY(valeur) {
        this.Y = valeur;
    }

}

class subCourbeBezier {

    constructor(points) {
        this.Points = points;
        this.SubPoints = this.calcSubPoints()
        this.ApproxLength = this.calcApproxLength()

    }

    calcSubPoints() {
        let temp = [this.Points[0]]
        
        for(let t = 0.01; t<1.0; t+=0.01) { // division de la courbe en 100 points
            let midAB = VecLerp(this.Points[0], this.Points[1], t);
            let midBC = VecLerp(this.Points[1], this.Points[2], t);
            let midCD = VecLerp(this.Points[2], this.Points[3], t);
            let ABBC = VecLerp(midAB, midBC, t);
            let BCCD = VecLerp(midBC, midCD, t);

            let pointFinal = VecLerp(ABBC, BCCD, t);
            temp.push(pointFinal);
        }
        
        temp.push(this.Points[3]);
        return temp;
    }

    calcApproxLength() {
        let temp = 0;
        for(let i=0; i<this.SubPoints.length-1;i++) {
            temp += this.SubPoints[i].copy().sub(this.SubPoints[i+1]).longueur;
        }
        return temp;
    }

    get approxLength() {
        return this.ApproxLength;
    }
    get subPoints() {
        return this.SubPoints;
    }
    get points() {
        return this.Points;
    }
}

class CourbeLineaire { //magnifique oxymore dans le nom de la classe :) ##CLASSE COMPLÈTE !

    constructor(hitObject) {

        this.Origine = new Vec2f(hitObject.getSliderPointX(0), hitObject.getSliderPointY(0));
        this.Fin = new Vec2f(hitObject.getSliderPointX(1), hitObject.getSliderPointY(1));
    }


    draw() {
        
        noFill();
        let coord = [scaleX(this.Origine.x), scaleY(this.Origine.y), scaleX(this.Fin.x), scaleY(this.Fin.y)];    
        strokeWeight(baseSize);
        stroke(sliderColor);
        line(coord[0], coord[1], coord[2], coord[3]);
        noStroke();
    }
    
    pointAt(t) {
        let vecteur = this.Fin.copy().sub(this.Origine);
        vecteur.scale(t);
        return this.Origine.copy().add(vecteur);
    }



}

class CourbeBezier { //fusion de Curve, EqualDistanceMultiCurve et LinearBezier

    constructor(hitObject) {
            
        //constructeur de Curve
        this.hitObject = hitObject;

        this.SubCourbes = [];
        for(let i=0; i<this.hitObject.sliderPointsX.length-2;i+=3) { 
            let point1 = new Vec2f(this.hitObject.getSliderPointX(i), this.hitObject.getSliderPointY(i));
            let point2 = new Vec2f(this.hitObject.getSliderPointX(i+1), this.hitObject.getSliderPointY(i+1));
            let point3 = new Vec2f(this.hitObject.getSliderPointX(i+2), this.hitObject.getSliderPointY(i+2));
            let point4 = new Vec2f(this.hitObject.getSliderPointX(i+3), this.hitObject.getSliderPointY(i+3));
            this.SubCourbes.push(new subCourbeBezier([point1, point2, point3, point4]));
        } 

        let longueurPixel = this.hitObject.longueurPixel;
        
        this.ApproxLength = 0;
        for(let x=0;x<this.SubCourbes.length;x++) {
            this.ApproxLength += this.SubCourbes[x].approxLength;
        }
        console.log("Longueur Pixel:", longueurPixel, "Distance Calculée:", this.ApproxLength, " Erreur :", longueurPixel-this.ApproxLength);
    }   

    pointAt(t) {

        t = Math.round(t * 100);
        return this.SubCourbes[0].SubPoints[t].copy();
    }

    draw() {    
        for(let i=0; i<this.SubCourbes.length;i++) {  
            let coordX = [scaleX(this.SubCourbes[i].points[0].x), scaleX(this.SubCourbes[i].points[1].x) ,scaleX(this.SubCourbes[i].points[2].x), scaleX(this.SubCourbes[i].points[3].x)];
            let coordY = [scaleY(this.SubCourbes[i].points[0].y), scaleY(this.SubCourbes[i].points[1].y) ,scaleY(this.SubCourbes[i].points[2].y), scaleY(this.SubCourbes[i].points[3].y)];
            noFill();
            strokeWeight(baseSize);
            stroke(sliderColor);
            bezier(coordX[0], coordY[0], coordX[1], coordY[1], coordX[2], coordY[2], coordX[3], coordY[3]);
            noStroke();
        }
    }

    get subCourbes() {
        return this.SubCourbes;
    }


}

class CourbeParfaite { //courbe circulaire ##ClASSE COMPLÈTE !

    constructor(hitObject) {

        let TWO_PI = Math.PI * 2;

        this.hitObject = hitObject;
        this.start = new Vec2f(hitObject.getSliderPointX(0), hitObject.getSliderPointY(0));
        this.mid = new Vec2f(hitObject.getSliderPointX(1), hitObject.getSliderPointY(1));
        this.end = new Vec2f(hitObject.getSliderPointX(2), hitObject.getSliderPointY(2));

        let midA = this.start.midPoint(this.mid);
        let midB = this.end.midPoint(this.mid);
        let norA = this.mid.copy().sub(this.start).nor();
        let norB = this.mid.copy().sub(this.end).nor();        
        this.circleCenter = this.intersect(midA, norA, midB, norB);



        this.startAngPoint = this.start.copy().sub(this.circleCenter);
        this.midAngPoint = this.mid.copy().sub(this.circleCenter);
        this.endAngPoint = this.end.copy().sub(this.circleCenter);
        this.startAng = Math.atan2(this.startAngPoint.y, this.startAngPoint.x);
        this.midAng = Math.atan2(this.midAngPoint.y, this.midAngPoint.x);
        this.endAng = Math.atan2(this.endAngPoint.y, this.endAngPoint.x);

        if(!this.isIn(this.startAng, this.midAng, this.endAng)) {
            if(Math.abs(this.startAng + TWO_PI -this.endAng)  < TWO_PI && this.isIn(this.startAng + TWO_PI, this.midAng, this.endAng)) {
                this.startAng+= TWO_PI;
            } else if (Math.abs(this.startAng - (this.endAng + TWO_PI)) < TWO_PI && this.isIn(this.startAng, this.midAng, this.endAng + TWO_PI)) {
                this.endAng += TWO_PI;
            } else if (Math.abs(this.startAng - TWO_PI - this.endAng) < TWO_PI && this.isIn(this.startAng - TWO_PI, this.midAng, this.endAng)) {
                this.startAng -= TWO_PI;
            } else if (Math.abs(this.startAng - (this.endAng - TWO_PI)) < TWO_PI && this.isIn(this.startAng, this.midAng, this.endAng - TWO_PI)) {
                this.endAng -= TWO_PI;
            }else {
                alert("Erreur lors du calcul des angles d'une courbe Parfaite !")
            }

        }
        
        this.radius = this.startAngPoint.longueur;
        
        let longueurPixel = hitObject.longueurPixel;
        let arcAng = longueurPixel / this.radius;

        if(this.endAng > this.startAng) {
            this.endAng = this.startAng + arcAng;
        } else {
            this.endAng = this.startAng - arcAng;
        }

        this.drawStartAng = this.startAng;
        this.drawEndAng = this.endAng;
                    
        if(this.startAng - this.endAng > 0) {
            let copy = this.drawStartAng;
            this.drawStartAng = this.drawEndAng;
            this.drawEndAng = copy;
        }


    }
    isIn(a, b, c) {
        return (b > a && b < c) || (b < a && b > c);
    }
    intersect(a, ta, b, tb) {
        let des = tb.x * ta.y - tb.y * ta.x;
        if (Math.abs(des) < 0.00001) {
            console.log("FatalException : les vecteurs sont parallèles : implementer LinearBezier à la place !");
        }
        else {
            let u = ((b.y - a.y)*ta.x + (a.x-b.x) *ta.y)/des;
            return b.copy().add(new Vec2f(tb.x, tb.y).scale(u));
        }
    }
    pointAt(t) {
        let ang = lerp(this.startAng, this.endAng, t);
        return new Vec2f(Math.cos(ang)*this.radius+this.circleCenter.x, Math.sin(ang)*this.radius+this.circleCenter.y);
    }



    draw() {                
                    
            noFill();
            stroke(sliderColor);
            strokeWeight(baseSize);                 
            let centre = new Vec2f(scaleX(this.circleCenter.x), scaleY(this.circleCenter.y));  
            let rayon  = new Vec2f(scaleX(this.hitObject.x), scaleY(this.hitObject.y)).sub(centre).longueur;

            arc(centre.x, centre.y, 2* rayon, 2*rayon, this.drawStartAng, this.drawEndAng);
            noStroke();

    }

}