//Fichier contenant les classes permettant le stockahe ordonné des données

class HitObject {
    constructor(ligne) {
           
		let variables = ligne.split(',');
		
		//variables pour tout les types de hitObject
		this.X = parseFloat(variables[0]);
        this.Y = parseFloat(variables[1]);
        this.Time = parseInt(variables[2]);
		this.Type = parseInt(variables[3]); // 1 = circle (5 circle New Combo), 2 = slider (6 slider New Combo), 8 = cercle
        this.HitSound = parseInt(variables[4]);
		this.NewCombo = false; 
		this.ApproachScale = 2;

		
		//Variables pour slider
		this.SliderType = null;
		this.SliderX = null;
		this.SliderY = null;
		this.Repetition = 0;
		this.LongueurPixel = 0;
		
		
		let additionIndex = 5;

        if (this.Type == 5) { //si c'est un cercle simple nouveau combo
        	this.NewCombo = true;
        	this.Type = 1;
        }
        else if (this.Type == 6) { //si c'est un slider nouveau combo
        	this.NewCombo = true;
			this.Type = 2;
		}
		else if(this.Type == 12) {
			this.NewCombo = true;
			this.Type = 8;
		}
		
		
		if(this.Type == 2) {
					
			additionIndex = 10;

			let variablesSlider = variables[5].split("|");
			this.SliderType = variablesSlider[0].charAt(0);
			this.SliderX = Array(variablesSlider.length -1);
			this.SliderY = Array(variablesSlider.length -1);
			
			for(let i=1; i<variablesSlider.length; i++) {
				let coordSlider = variablesSlider[i].split(":");
				this.SliderX[i - 1] = parseInt(coordSlider[0]); 
				this.SliderY[i - 1] = parseInt(coordSlider[1]);
			}

			this.Repetition = parseInt(variables[6]);
			this.LongueurPixel = parseFloat(variables[7]);

			this.PointInitialClick = false;
			this.SliderMappedTouche = null;

			// @TODO : Implementer chargement des edgeAddition et edgeHitSound

			if(this.SliderType == 'L') {
				this.Curve = new CourbeLineaire(this);
			}
			else if (this.SliderType == 'P') {
				let norA = new Vec2f(this.SliderX[0] -this.X, this.SliderY[0]- this.Y).nor();
				let norB = new Vec2f(this.SliderX[0] - this.SliderX[1], this.SliderY[0] - this.SliderY[1]).nor();
				if (Math.abs(norB.x * norA.y - norB.y * norA.x) < 0.00001) {
					this.Curve = new CourbeBezier(this, false);
				} else {
					this.Curve = new CourbeParfaite(this);
				}
			} else {
				this.Curve = new CourbeBezier(this, false);
			}


		}
		else if(this.Type == 8) {
			additionIndex = 6;

			let index = variables[5].indexOf(':');
			if(index != -1){
				variables[5] = variables[5].substring(0, index);
			}
			this.EndTime = parseInt(variables[5]);

		}
        
        

        
        
        
        
        	   
//	 Code à Porter pour les hitsounds custom à chaque point et les combos de couleurs
       	
        }  
		
	//Getters
	get isNewCombo() {
		return this.NewCombo;
	}
	get comboColorIndex() {
		return this.ComboColorIndex;
	}
	get comboIndex() {
		return this.ComboIndex;
	}
    get x() {
        return this.X; 
    }
    get y() {
        return this.Y; 
	}
    get time() {
        return this.Time;
    }
    get approachScale() {
    		return this.ApproachScale;
    }
    get type() {
    	return this.Type;
    } 
    get newCombo() {
    	return this.NewCombo;
	}
	
	//getters spécifique aux slider
	get repetition() {
		return this.Repetition;
	}
	get longueurPixel() {
		return this.LongueurPixel;
	}
	
	get tempsSlider() {
		return this.BeatLength * (this.LongueurPixel / this.MultiplicateurSlideur) / 100.0;
	}
	get tempsTotalSlider() {
		return this.tempsSlider * this.Repetition;
	}

	get sliderPointsX() {
		return this.SliderX;
	}
	get sliderPointsY() {
		return this.SliderY;
	}
	getSliderPointX(index) {
		if (index == 0) {
			return this.X;
		} else {
			return this.SliderX[index-1];
		}
	}
	getSliderPointY(index) {
		if (index == 0) {
			return this.Y;
		} else {
			return this.SliderY[index -1];
		}
	}
	get sliderType() {
		return this.SliderType;
	}
	
	//fonction de mise à jour de la taille des cercles d'approche + vérification du cliques    
    Update(tempsEnJeu, tempsApproche) {
		this.ApproachScale = Math.max(1 + 3 * ((this.Time - tempsEnJeu) / tempsApproche), 1);
	}	
	setComboIndex(index) {
		this.ComboIndex = index;
	}              
    setComboColorIndex(index) {
		this.ComboColorIndex = index;
	}
	setSliderMultiplier(sliderMultiplier) {
		this.MultiplicateurSlideur = sliderMultiplier;
	}
	setBeatLength(beatLength) {
		this.BeatLength = beatLength;
	}

	get curve() {
		return this.Curve;
	}

	get endTime() {
		if (this.Type == 8) {
			// TODO: return this.
			return this.EndTime;
		} else if(this.Type == 2) {
			return this.Time + this.tempsTotalSlider;
		}
		return this.Time;
	}

	get pointInitialClick() {
		return this.PointInitialClick;
	}
	setPointInitialClick(valeur) {
		this.PointInitialClick = valeur;
	}

}
class TimingPoint {

	constructor(ligne) {
	
		let variables = ligne.split(",");
		this.Time = variables[0];
		this.Meter = parseInt(variables[2]);
		this.SampleType = parseInt(variables[3]);
		this.SampleTypeCustom = parseInt(variables[4]);
		this.SampleVolume = parseInt(variables[5]);
				
		this.Kiai = false;
		if (variables.length > 7) {
			this.Kiai = true;
		}
		
				
		this.BeatLength = 0;
		this.Inherited = false;
		this.Velocity = 0;	
		
		let beatLength = 0;
		beatLength = parseFloat(variables[1]);
		if(beatLength > 0) {
			this.BeatLength = beatLength;
		}
		else {
			this.Velocity = Math.round(beatLength);
			this.Inherited = true;
		}	
	}
	
	get time() {
		return this.Time
	}
	get inherited() {
		return this.Inherited;
	}
	get beatLength() {
		return this.BeatLength;
	}
	get sliderMultiplier() {
		let min = Math.max(-this.Velocity, 10);
		return Math.min(min, 1000) / 100.0;
	}
}
class BeatMap {
    constructor(folder, file) {	  
		   
		   var rawFile = new XMLHttpRequest();    
		   var charger = function(classe, fichierOSU) { 
                                 
            //variables de [General] :
            let NomFichierAudio = 'audio.mp3';            
            let OffsetAudio = 0;
	      	let ExtraitTime = 0;
          
            //variables de [Event]:
            let ImageFondNomFichier = "background.png";
          
            //variable de [Metadata] :
            let Titre = "Titre";
            let Artiste = "Artiste";
            let Version = "Version";
            let Createur = "Créateur";
            
            //variables de [Difficulty] :
            let VitessePertePV = 0;
            let TailleCercle = 0;
            let DifficulteeGenerale = 0;
            let VitesseApproche = 6;
            let MultiplicateurSlideur = 0.0;
            let TauxTickSlideur = 0;

	    	let HitObjects = [];
	    	let HitResultsOffset = []; //dans l'odre offset pour: 300,100,50,miss.
	    	let CouleursCombo = [color(206,32,101), color(117,23,204), color(23,53,204), color(23,130,204), color(23,204,100)];

            let TimingPoints = [];
                        
            //Lecture du fichier 
            let lignes = fichierOSU.split('\r\n');
            let i = 0;
            while (i < lignes.length) {
            	

            	
            	switch (lignes[i]) {
            		case "[General]":
            			//lecture de toutes les variables
            			while(lignes[i] != "" && i < lignes.length) {
            				let variables = lignes[i].split(': ');
            				switch (variables[0]) {
            					case "AudioLeadIn":
            						OffsetAudio = variables[1];
            					break;
            					
            					case "AudioFilename":
            						NomFichierAudio = variables[1];
            					break;
            					case "PreviewTime":
            						ExtraitTime = parseInt(variables[1]);
            					break;
            					default:
            					break;
            				} 
            				i++;          				

            			
            			}
            		break;
            		
            		case "[Metadata]":
				//lecture de toutes les variables
            			while(lignes[i] != "" && i < lignes.length) {
            				let variables = lignes[i].split(':');
            				switch (variables[0]) {
            					case "Title":
            						Titre = variables[1];
            					break;
            					case "TitleUnicode":
            						if (variables[1] != undefined) {Titre = variables[1];}
            					break;
            					case "Artist":
            						Artiste = variables[1];
            					break;
            					case "ArtistUnicode":
            						if (variables[1] != undefined) {Artiste = variables[1];}
            					break;
            					case "Version":
            						Version = variables[1];
            					break;
            					case "Creator":
            						Createur = variables[1];
            					break;
            					default:
            					break;
            				} 
            				i++;          				
            			
            			}           		
            		
            		break;
            		
            		case "[Difficulty]":
				//lecture de toutes les variables
            			while(lignes[i] != "" && i < lignes.length) {
            				let variables = lignes[i].split(':');
            				switch (variables[0]) {
            					case "HPDrainRate":
            						VitessePertePV = parseInt(variables[1]);
            					break;
            					case "CircleSize":
            						TailleCercle = parseInt(variables[1]);
            					break;
            					case "OverallDifficulty":
            						DifficulteeGenerale = parseInt(variables[1]);
            					break;
            					case "ApproachRate":
            						VitesseApproche = parseInt(variables[1]);
            					break;
            					case "SliderMultiplier":
            						MultiplicateurSlideur = parseFloat(variables[1]);
            					break;
            					case "SliderTickRate":
            						TauxTickSlideur = parseInt(variables[1]);
            					break;
            					default:
            					break;
            				} 
            				i++;          				
            			
            			}
            		
            		break;
			
					case "[TimingPoints]":
            			i++;
            			while( lignes[i] != "") {
            				TimingPoints.push(new TimingPoint(lignes[i]));
            				i++;
            			}	
            		
            		break;
            		case "[HitObjects]":
            			i++;
            			while( i < lignes.length-1) {
							HitObjects.push(new HitObject(lignes[i]));
            				i++;
            			}
            			
            		break;
            		
            		case "[Events]":
            			let variables = lignes[i+2].split(',');
            			ImageFondNomFichier = variables[2].slice(1, variables[2].length-1);
            		break;
            		
            		case "[Colours]":
            			CouleursCombo = [];
            			i++;
            			while(lignes[i] != "" && i < lignes.length) {
            				let variables1 = lignes[i].split(' : ');
            				let variables2 = variables1[1].split(',');
            				CouleursCombo.push(color(parseInt(variables2[0]), parseInt(variables2[1]), parseInt(variables2[2])));		
            				 i++;	
            				}           				
            		break;	
            		           		
            		
            		default: 
            		break;    	
            	}
            	i++;
            }


            
            parent.audio = new Audio("Beatmaps/" + folder + "/" + NomFichierAudio, true);
	  	 	parent.audio.preload = true;
	  	  	parent.audio.loop = true;
	   	 	parent.audio.currentTime = ExtraitTime/1000;

            parent.Imagefond = "Beatmaps/" + folder + "/" + ImageFondNomFichier;
            
            
            parent.OffsetAudio = OffsetAudio;
            parent.NomFichierAudio = NomFichierAudio;
            parent.ExtraitTime = ExtraitTime;
            parent.Createur = Createur;
            parent.Version = Version;
            parent.Titre = Titre;
            parent.Artiste = Artiste;
            
            parent.VitessePertePV = VitessePertePV;
            parent.TailleCercle = TailleCercle;
            parent.DifficulteeGenerale = DifficulteeGenerale;
            parent.VitesseApproche = VitesseApproche;
            parent.TempsApproche =  mapDifficultyRange(VitesseApproche, 1800,1200,450);
            parent.MultiplicateurSlideur = MultiplicateurSlideur;
            parent.TauxTickSlideur = TauxTickSlideur;
           
            HitResultsOffset = Array(4);                 
            HitResultsOffset[0]  = mapDifficultyRange(DifficulteeGenerale, 80, 50, 20);
	   		HitResultsOffset[1]  = mapDifficultyRange(DifficulteeGenerale, 140, 100, 60);
	   		HitResultsOffset[2]  = mapDifficultyRange(DifficulteeGenerale, 200, 150, 100);
            HitResultsOffset[3]  = 500 - (DifficulteeGenerale * 10);            
            parent.HitResultsOffset = HitResultsOffset;
            
            parent.DiametreCercle = 108.848 - (TailleCercle * 8.9646); //en osu pixel (640x480) !!
					
            parent.HitObjects = HitObjects;
            parent.CouleursCombo = CouleursCombo;
	   		parent.TimingPoints = TimingPoints;
	    

	    	let colorIndex = 0;
	   		let comboIndex = 1;

			for (let i=0; i<parent.HitObjects.length;i++) {
				if(parent.HitObjects[i].isNewCombo == false) {
					parent.HitObjects[i].setComboColorIndex(colorIndex);
					parent.HitObjects[i].setComboIndex(comboIndex);
					comboIndex++;
				}
				else {
					comboIndex = 1;
					colorIndex++;
					if (colorIndex >= parent.CouleursCombo.length) {
						colorIndex = 0;
					}		
					parent.HitObjects[i].setComboColorIndex(colorIndex);
						parent.HitObjects[i].setComboIndex(comboIndex);	
						comboIndex++;
				
				}
	   	 	}
            let event = new Event("beatmap_loaded");
            document.dispatchEvent(event);
          
        }
        
        var parent = this;
        rawFile.onload = function() { charger(parent, this.responseText);} 
        rawFile.open("GET", "Beatmaps/" + folder + '/' + file);
        rawFile.overrideMimeType("text/plain");
        rawFile.send(); 
    }
      
    Update(tempsEnJeu, indexOfLastHitObject, indexOfFirstHitObject) {
	    	
		//Récup index du nouveau plus récent point dont le temps implioque qu'il devrait être affiché
		if(indexOfFirstHitObject != this.HitObjects.length-1) {
			while(this.HitObjects[indexOfFirstHitObject].time < tempsEnJeu+this.TempsApproche) {
				indexOfFirstHitObject++;
				if(indexOfFirstHitObject >= this.HitObjects.length) {
					break;
				}
			}
			indexOfFirstHitObject--;
			indexOfFirstHitObject =  Math.max(  Math.min(this.HitObjects.length-1, indexOfFirstHitObject),   0);
		}

		//mise à jour des points (cercle d'approche)
		for (let j=indexOfLastHitObject;j<=indexOfFirstHitObject;j++) {
			this.HitObjects[j].Update(tempsEnJeu, this.TempsApproche);
		}
		
		
		
		return indexOfFirstHitObject;
    }
       
    

    
    //Getters
    get musique() {
        return this.audio;
    }
    get background() {
        return this.Imagefond;
    }
    get offsetAudio() {
    	return this.OffsetAudio;
    }
    
    get artiste() {
    	return this.Artiste;
    }
    get titre() {
    	return this.Titre;
    }
    get createur() {
    	return this.Createur;
    }
    get version() {
    	return this.Version;
    }
    get extraitTime() {
    	return this.ExtraitTime;
    }
    get vitesseApproche() {
    	return this.VitesseApproche;
    }
    get vitessePertePV() {
    	return this.VitessePertePV;
    }
    get tailleCercle() {
    	return this.TailleCercle;
    }
    get difficulteeGenerale() {
    	return this.DifficulteeGenerale;
    }
    get multiplicateurSlideur() {
    	return this.MultiplicateurSlideur;
    }
    get tauxTickSlideur() {
    	return this.TauxTickSlideur;
    } 
    get tempsApproche() {

    	return this.TempsApproche;
    }
    get couleursCombo() {
    	return this.CouleursCombo;
    }
    get hitResultsOffset() {
    	return this.HitResultsOffset;
    }
    get diametreCercle() {
    	return this.DiametreCercle; //en Osu Pixel (640*480 !!)
    } 
    get hitObjectsLength() {
    	return this.HitObjects.length;
    }
    get timingPointsLength() {
    	return this.TimingPoints.length;
	}
	get hitObjects() {
		return this.HitObjects;
	} 
    getObjets(indexOfLastHitObject, indexOfFirstHitObject) {
    	return this.HitObjects.slice(indexOfLastHitObject, indexOfFirstHitObject+1);
    }
    getObjet(index) {
    	return this.HitObjects[index];
    }
    getTimingPoint(index) {
    	return this.TimingPoints[index];
    }
}
class Score {

	constructor() {
		this.Reset();
	}
	
	//Restauration des variables évoluant aux cours de la partie
	Reset() {
		
		//Variblables générales
		this.Score = 0;
		this.Score_Brut = 0;
		this.Score_Brut_Max = 0;
		this.Combo = 0;
		this.Nbr300 = 0;
		this.Nbr100 = 0;0
		this.Nbr50 = 0;
		this.NbrMiss = 0;
		this.NbrObjets = 0;	
	}
	
	//Mise à jour du score après une note
	Update(hitResult) {			
		switch (hitResult) {
			case 0:
				this.Score = this.Score + 300*this.Combo;
				this.Score_Brut = this.Score_Brut + 300;
				this.Combo++;
				this.Nbr300++;	
			break;
			
			case 1:			
				this.Score = this.Score + 100*this.Combo;
				this.Score_Brut = this.Score_Brut + 100;
				this.Combo++;
				this.Nbr100++;
			break;
			
			case 2:
				this.Score = this.Score + 50*this.Combo;
				this.Score_Brut = this.Score_Brut + 50;
				this.Combo++;
				this.Nbr50++;
			break;
			
			default:
				this.NbrMiss++;
				this.Combo = 0;
			break;
		
		
		}
		
		this.Score_Brut_Max = this.Score_Brut_Max+300;
		this.NbrObjets++;
	
	}



	//Getters
	get score() {
		return this.Score;
	}
	get combo() {
		return this.Combo;
	}
	get score_brut() {
		return this.Score_Brut;
	}
	get score_brut_max() {
		return this.Score_Brut_Max;
	}
	get precision() {
		let accu = (100 * (this.Score_Brut/this.Score_Brut_Max)).toFixed(2);
		if (0 == this.score_brut_max) {
			return 100;
		} else {		
			return accu; 
		}

	}
	
	get grade() {
	// revoie le grade : 0 = D, 1 = C, 2 = B, 3 = A, 4 = S, 5=SH (S Hidden),  6 = SS, 7 = SSH (SS Hidden)
		if (this.NbrObjets < 1)  // avoid division by zero
			return null;

		let accu = this.precision;
		let hit300ratio = this.Nbr300 / this.NbrObjets * 100;
		let hit50ratio = this.Nbr50 / this.NbrObjets * 100;
		let noMiss = (this.NbrMiss == 0);

		
		if (accu >= 100) { 		
			return 6; 
		}		
		else if (hit300ratio >= 90 && hit50ratio < 1.0 && noMiss) {  			
			return 4;
		}
		else if ( (hit300ratio >= 80 && noMiss) || hit300ratio >= 90) {			
			return 3;
		}
		else if ( (hit300ratio >= 70 && noMiss) || hit300ratio >= 80) {			
			return 2;
		}
		else if (hit300ratio >= 60) {
			return 1;
		}
		else {
			return 0;
		}
	}
}
class Vie {

	constructor() {

		this.Reset();
		
		//variables pour le calcul de la vie
		this.coef50 = 0.4;
		this.coef100=2.2;
		this.coef300=6.0;
		this.coef300G=14.0;
		this.coefSlider30=4;
		this.coefSlider10=3;
		
		this.vitessePertePV = 5;
		this.tauxDecrochagePV = 0.05;
		this.multiplierNormalPV = 1;
		this.multiplierFinComboPV = 1;
		
	}
	
	//Restauration des variables évoluant aux cours de la partie
	Reset() {
		//Variables pour la vie
		this.Vie = 200;
		this.VieUncapped = 200;
		
		//Variables pour le calcul du score
		this.Nbr300 = 0;
		this.Nbr50 = 0;
		this.NbrMiss = 0;
		this.NbrObjets = 0;
		
		this.lastTempsEnJeu = this.startTime;
	
	}
	
	//Paramétrages des variables variant selon la beatmap sélectionnée et chargée
	Init(beatmap) {		
		
		this.startTime = beatmap.getObjet(0).time;
		
		let lowestHpEver = mapDifficultyRange(beatmap.vitessePertePV, 195, 160, 60);
		let lowestHpComboEnd = mapDifficultyRange(beatmap.vitessePertePV, 198, 170, 80);
		let lowestHpEnd = mapDifficultyRange(beatmap.vitessePertePV, 198, 180, 80);
		let hpRecoveryAvailable = mapDifficultyRange(beatmapList.vitessePertePV, 8, 4, 0);

		let testDrop = 0.05;
		
		this.vitessePertePV = beatmap.vitessePertePV;
		let multiplierNormalPV = 1.0;
		let multiplierFinComboPV = 1.0;
				
		while (true) {
						
			this.Reset();	
			
			this.multiplierNormalPV = multiplierNormalPV;
			this.multiplierFinComboPV = multiplierFinComboPV;
			
			let lowestHp = this.Vie;
			let lastTime = beatmap.getObjet(0).time - beatmap.tempsApproche;
			let comboTooLowCount = 0;
			let fail = false;
			let timingPointIndex = 0;
			let beatLengthBase = 1.0 
			let beatLength = 1;

			for (let i = 0; i < beatmap.hitObjectsLength; i++) {
				let hitObject = beatmap.getObjet(i);

//				
// breaks
//				breakTime = 0;
//				if (beatmap.breaks != null) {
//					for (int j = 0; j < beatmap.breaks.size(); j += 2) {
//						int breakStart = beatmap.breaks.get(j), breakEnd = beatmap.breaks.get(j+1);
//						if (breakStart >= lastTime && breakEnd <= hitObject.getTime()) {
//							breakTime = breakEnd - breakStart;
//							break;
//						}
//					}
//				}
				
				//TODO health.changeHealth(-testDrop * (hitObject.time - lastTime - breakTime));
				
				this.changerVie(-testDrop * (hitObject.time - lastTime));

				let hitObjectTime = hitObject.time;
				while (timingPointIndex < beatmap.timingPointsLength) {
					let timingPoint = beatmap.getTimingPoint(timingPointIndex);
					
					
					if ((timingPoint.time) > hitObjectTime) {
						break;
					}

					if (!timingPoint.inherited) {
						beatLengthBase = timingPoint.beatLength;
						beatLength = timingPoint.beatLength;
					}	
					else {
						beatLength = beatLengthBase * timingPoint.sliderMultiplier;
					}
					timingPointIndex++;
				}

				
				hitObject.setSliderMultiplier(beatmap.multiplicateurSlideur);
				hitObject.setBeatLength(beatLength);

				let endTime = hitObject.endTime;
				lastTime = endTime;

				if (this.Vie < lowestHp)
					lowestHp = this.Vie;

				if (this.Vie <= lowestHpEver) {
					fail = true;
					testDrop *= 0.96;
					break;
				}

				this.changerVie(-testDrop * (endTime - hitObject.time));
				
				if (hitObject.type == 2) {
					let tickLengthDiv = 100 * beatmap.sliderMultiplier / beatmap.sliderTickRate / (beatLength / beatLengthBase);
					let tickCount =  Math.ceil(hitObject.longueurPixel / tickLengthDiv) - 1;
					for (let j = 0; j < hitObject.repetition; j++)
						this.UpdateClick(5); //POUR SLIDER 30
					for (let j = 0; j < tickCount * hitObject.repetition; j++)
						this.UpdateClick(6);
				}
				/*  else if (hitObject.isSpinner()) {
//					float spinsPerMinute = 100 + (beatmap.overallDifficulty * 15);
//					int rotationsNeeded = (int) (spinsPerMinute * (hitObject.getEndTime() - hitObject.getTime()) / 60000f);
//					for (int j = 0; j < rotationsNeeded; j++)
//						health.changeHealthForHit(GameData.HIT_SPINNERSPIN);
//				} */
				
				this.UpdateClick(0);
				if (i == beatmap.hitObjectsLength - 1 || beatmap.getObjet(i + 1).newCombo) {
					this.UpdateClick(4);
					if (this.Vie < lowestHpComboEnd) {
						if (++comboTooLowCount > 2) {
							fail = true;
							multiplierNormalPV *= 1.03;
							multiplierFinComboPV *= 1.07;
							break;
						}
					}
				}
			
			
			
			}


			if (!fail && this.Vie < lowestHpEnd) {
				fail = true;
				testDrop *= 0.94;
				multiplierNormalPV *= 1.01;
				multiplierFinComboPV *= 1.01;
			}

			let recovery = (this.VieUncapped - 200) / beatmap.hitObjectsLength;
			if (!fail && recovery < hpRecoveryAvailable) {
				fail = true;
				testDrop *= 0.96;
				multiplierNormalPV *= 1.01;
				multiplierFinComboPV *= 1.02;
			}

			if (fail)
				continue;
				
			this.tauxDecrochagePV = testDrop;
			break;
		}
		
	}

	//Fonction de modifications de la vie restante
	changerVie(delta) {
		let temp1 = this.Vie + delta;
		this.Vie = Math.min( Math.max(temp1, 0), 200);
		this.VieUncapped = this.VieUncapped + delta;
				
	}
	UpdateClick(hitResult) {	
		
		
		switch (hitResult) {
			case 0: //300
				this.changerVie(this.multiplierNormalPV * this.coef300);
				break; 
			
			case 1: //100			
				this.changerVie(this.multiplierNormalPV * mapDifficultyRange(this.vitessePertePV, this.coef100*8, this.coef100, this.coef100));
				break;
			
			case 2: //50
				this.changerVie(this.multiplierNormalPV * mapDifficultyRange(this.vitessePertePV, this.coef50*8, this.coef50, this.coef50));	
				break;
			
			case 4: //300G
				this.changerVie(this.multiplierNormalPV * this.coef300G);
				break;

			case 5: //Slider30
				this.changerVie(this.multiplierNormalPV * this.coefSlider30);
				break;
			case 6: //slider 10
				this.changerVie(this.multiplierNormalPV * this.coefSlider10);
				break;
			case 3: //miss
				this.changerVie(mapDifficultyRange(this.vitessePertePV, -6, -25, -40));
				break;

			default:
				break;
		
		}
	
	}	
	UpdatePassive(tempsEnJeu) {

		this.changerVie( (tempsEnJeu - this.lastTempsEnJeu) * -1 * this.tauxDecrochagePV);
		this.lastTempsEnJeu = tempsEnJeu;
	}

	//Getters
	get vie() {
		return this.Vie;
	}

}
class Particule {

	constructor(type, tempsApparition, coordX, coordY) {
		this.Type = type;
		this.Time = tempsApparition;
		this.X = coordX;
		this.Y = coordY;
	
	}

	get type() {
		return this.Type;
	}
	get time() {
		return this.Time;
	}
	get x() {
		return this.X;
	}
	get y() {
		return this.Y;
	}
}