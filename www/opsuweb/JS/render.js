// FONCTIONS DE RENDU D'OBJETS

function drawCercle(hitObject) {
    
    x = scaleX(hitObject.x);
    y = scaleY(hitObject.y); 
 
    couleur = couleursCombo[hitObject.comboColorIndex];
    
    image(imageCercleOverlay, x-baseSize/2, y-baseSize/2, baseSize, baseSize);
    tint(couleur);
    image(imageCercle, x-baseSize/2, y-baseSize/2, baseSize, baseSize);
    size = baseSize*hitObject.approachScale;
    offset = size/2;
    image(imageCercleApproche, x-offset, y-offset, size, size);
    noTint();
   
    fill(255,255,255);
    textAlign(CENTER, CENTER);
    textSize(64);
    text(String(hitObject.comboIndex), x, y);


}
function drawSlider(hitObject) {
    
    // affichage de la courbe du slider
    hitObject.curve.draw();
    
    //calcul de la répétion du slider en cours
    let currentRepetion = Math.ceil( (tempsEnJeu-hitObject.time) / hitObject.tempsSlider);
    currentRepetion = Math.max(currentRepetion, 1);

    //cercle de début de slider
    if(currentRepetion == 1) {
        drawCercle(hitObject); 
    }
    
    // images de retour du slider -> manque de mettre une flếche et de calculer l'angle

    if(hitObject.repetition >= 2 && hitObject.repetition > currentRepetion) {
        if( currentRepetion%2 != 0) {
            endPoint = hitObject.curve.pointAt(1);
            endPoint.setX(scaleX(endPoint.x));
            endPoint.setY(scaleY(endPoint.y));
            size = baseSize/4;
            image(imageSliderScorePoint, endPoint.x-size/2, endPoint.y-size/2, size, size);
        }
        else {
            endPoint = hitObject.curve.pointAt(0);
            endPoint.setX(scaleX(endPoint.x));
            endPoint.setY(scaleY(endPoint.y));
            size = baseSize/4;
            image(imageSliderScorePoint, endPoint.x-size/2, endPoint.y-size/2, size, size);
        }
        


    }
    if(hitObject.repetition >= 2 && hitObject.repetition > currentRepetion+1) {
        if( (currentRepetion+1)%2 != 0) {
            endPoint = hitObject.curve.pointAt(1);
            endPoint.setX(scaleX(endPoint.x));
            endPoint.setY(scaleY(endPoint.y));
            size = baseSize/4;
            image(imageSliderScorePoint, endPoint.x-size/2, endPoint.y-size/2, size, size);
        }
        else {
            endPoint = hitObject.curve.pointAt(0);
            endPoint.setX(scaleX(endPoint.x));
            endPoint.setY(scaleY(endPoint.y));
            size = baseSize/4;
            image(imageSliderScorePoint, endPoint.x-size/2, endPoint.y-size/2, size, size);
        }
        


    }


    //code d'affichage du cercle de suivi : OK pour Linéaire et Circulaire
    let progressionTotale = (tempsEnJeu-hitObject.time) / hitObject.tempsTotalSlider;
    progressionTotale = Math.min(Math.max(progressionTotale, 0)    ,1);
    let progressionCourante = progressionTotale * hitObject.repetition;
    
    if(progressionTotale > 0) {    
        if(Math.floor(progressionCourante)%2 != 0) {
            progressionCourante = 1-(progressionCourante-Math.floor(progressionCourante));
        }
        else { //remise à l'intervalle 0/1 pour la progression
            progressionCourante = progressionCourante - 2*Math.floor(progressionCourante/2);
        }
        
        let pountos = hitObject.curve.pointAt(progressionCourante);
        image(imageSliderFollowCircle, scaleX(pountos.x)-baseSize, scaleY(pountos.y)-baseSize, baseSize*2, baseSize*2);
        
    }    
}
function drawSpinner(hitObject) {
    drawCercle(hitObject);
}


// FONCTIONS DE RENDU PRINCIPALES : Menu, UI, Beatmap

function drawPause() { //affichage du menu de pause
    
    fill(10,10,10,150); //affichage halo gris semi-transparent
    rect(0,0,windowWidth, windowHeight);
    
    fill(255,255,255); //Affichage texte pause
    textAlign(CENTER, CENTER);
    textSize(128);
    text('Pause', windowWidth/4, windowHeight/2);
    
    //affichage bouton du menu
    image(pause_back, windowWidth*0.6, windowHeight*5/16);
    image(pause_retry, windowWidth*0.6, windowHeight*8/16);
    image(pause_continue, windowWidth*0.6, windowHeight*11/16);
    
}
function drawMenu() { //affichage du menu   
    image(logo_img, windowWidth/2-logo_img.width/2,  windowHeight/2-logo_img.height/2, logo_img.width, logo_img.height);
}


//Fonction Macros

function drawPlaying() {    

    for (let j=0;j<particules.length;j++) { //affichage des particules 
   	    value = 255-((tempsEnJeu-particules[j].time)/750*255);
   	    tint(255,Math.max(value, 0));
   	    image(imagesHitResult[particules[j].type], xOffset + particules[j].x*xMultiplier-40, yOffset + particules[j].y*yMultiplier-31); 
        noTint();
    } 
    
    for (let i=objetsAafficher.length-1;i>=0;i--) {  //affichage des hitObjects en mettant les plus vieux au dessus
        if (objetsAafficher[i].type == 1) { //si l'hitObject est un cerlce
            drawCercle(objetsAafficher[i]);
        }
        else if (objetsAafficher[i].type == 2) { //si l'hitObject est un slider
            drawSlider(objetsAafficher[i]);
        }
        else if (objetsAafficher[i].type == 2) { //si l'hitObject est un spinner
            drawSpinner(objetsAafficher[i]);
        }

    }
    
    //mise à jour de l'UI
    document.getElementById('bas-gauche').innerHTML = String(score.combo); //combo
    document.getElementById('haut-droit').innerHTML = String(score.score); //score  

    grade = score.grade; //si le grade=null, alors la map n'as pas commencé donc précision = rien
    if (grade != null) {
        image(imagesGrades[grade], windowWidth-60, 120); //affichage du grade
        document.getElementById('haut-droit2').innerHTML = String(score.precision) + '%';  //précision
    }

         
    fill(0,0,255); //barre de vie
    rect(15,15,0.4*windowWidth*(vie.vie/200), 20);

    if (allow_skip == true && tempsEnJeu >= 0) {
        
        coordX = 0
        if (windowWidth < 1350) {
            coordX = windowWidth - 1350;
            sizeX = 1350
        }
        else {
            coordX = windowWidth - 1350;
        }

        image(imageSkip, coordX,windowHeight - 150, 1350, 150)

    }



    
}
function displayFPS() {
	let fps = frameRate();
	fps = parseInt(fps);
	document.getElementById('bas-droit').innerHTML = "Fps:" +  String(fps);
}
function draw() {
    
    if (started === true) {
        clear();
        if (playing === true) {
            Update();
            drawPlaying();
        }
        else if(playing === false) {
            drawMenu();
        }
            
        if (paused === true) {
            drawPause()
        } 
        displayFPS();
        }
          
}



