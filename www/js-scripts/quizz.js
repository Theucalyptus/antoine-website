

var quizzCheck = function() {

    let msgCorrect = "Vrai ✔";
    let msgFaux = "Faux ✗"
    let i = 0;
    let nom = document.getElementById("nom").value;

    value = document.getElementById("q1").value;
    if (value == "3") {
        document.getElementById("q1rep").innerText = msgCorrect;
        document.getElementById("q1rep").classList = "";
        document.getElementById("q1rep").classList.add("correct");
        i++;
    }
    else {
        document.getElementById("q1rep").innerText = msgFaux;
        document.getElementById("q1rep").classList = "";
        document.getElementById("q1rep").classList.add("faux");
    }

    value = document.getElementById("q2").value;
    if (value == "2007") {
        document.getElementById("q2rep").innerText = msgCorrect;
        document.getElementById("q2rep").classList = "";
        document.getElementById("q2rep").classList.add("correct");
        i++;
    }
    else {
        document.getElementById("q2rep").innerText = msgFaux;
        document.getElementById("q2rep").classList = "";
        document.getElementById("q2rep").classList.add("faux");
    }

    value = document.getElementById("q3").checked;
    if (value == false) {
        document.getElementById("q3rep").innerText = msgCorrect;
        document.getElementById("q3rep").classList = "";
        document.getElementById("q3rep").classList.add("correct");
        i++;
    }
    else {
        document.getElementById("q3rep").innerText = msgFaux;
        document.getElementById("q3rep").classList = "";
        document.getElementById("q3rep").classList.add("faux");
    }

    value = document.getElementById("q4").value;
    if (value == "4") {
        document.getElementById("q4rep").innerText = msgCorrect;
        document.getElementById("q4rep").classList = "";
        document.getElementById("q4rep").classList.add("correct");
        i++;
    }
    else {
        document.getElementById("q4rep").innerText = msgFaux;
        document.getElementById("q4rep").classList = "";
        document.getElementById("q4rep").classList.add("faux");
    }


    value = document.getElementById("q5").value;
    if (value == "2") {
        document.getElementById("q5rep").innerText = msgCorrect;
        document.getElementById("q5rep").classList = "";
        document.getElementById("q5rep").classList.add("correct");
        i++;
    }
    else {
        document.getElementById("q5rep").innerText = msgFaux;
        document.getElementById("q5rep").classList = "";
        document.getElementById("q5rep").classList.add("faux");
    }

    let compMSG = "";

    if (i == 5) {
        compMSG = "😄 Bravo " + nom + "!";
    }
    else if (i == 4) {
        compMSG = "😃 Félicitations " + nom + ", vous y êtes presque.";
    }
    else if (i==3) {
        compMSG = "🙂 Vous avez la moyenne, " + nom + ".";
    } else if ( i== 2) {
        compMSG = "😐 C'est limite, " + nom + '.';
    }
    else if (i==1) {
        compMSG = "😤 Ressaisissez vous, " + nom + "!"
    }
    else if (i==0){
        compMSG = "🤬 Les mots me manque pour décrire votre niveau, " + nom + ".";
    }

    document.getElementById("message").innerText = compMSG + " Vous avez obtenu la note de " + String(i) + "/5."




}