
window.onload = function() {

    var récupPageHTML = function(url) { //Fonction de chargement de page web externe asynchrone


            // création de la requête HTTP
            var requete = new XMLHttpRequest();

            //création du rappelle de fonction pour lorsque la page est arrivée
            requete.onload = function() { //quand la charge utile est arrivé
                    document.title = "Antoine Lebeault - " + this.responseXML.getElementById("titre").innerHTML;
                    document.getElementById("contenu").innerHTML = this.responseXML.getElementById("contenu").innerHTML;
                    window.scrollTo(0,0);
                }


            //lancement de la requete pour la page voulue
            requete.open('GET',  url)
            requete.responseType = 'document';
            requete.send();
    }

    //au chargement de la page pour la première fois, on charge l'acceuil par défaut
    récupPageHTML('content/pages/acceuil.html')

    	
    // on paramètre les appels au script lors de la sélection d'une page dans le menu
    var entrees = document.getElementsByClassName('entree-navbar');
    for (let i = 0; i < entrees.length; i++) {
        entrees.item(i).onclick = function() {
           récupPageHTML(entrees[i].getAttribute('href', 2));
           return false;
        }
    }

    document.getElementById("scroll-bouton").onclick = function() {
        window.scrollTo(0,0);
        document.getElementById("header").classList.remove("scroll");
        document.getElementById("header").classList.add("descroll");
        if (window.scrollY != 0) {
            scrolling = true; }
    }
    
}


